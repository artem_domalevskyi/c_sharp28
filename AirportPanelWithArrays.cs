﻿using System;
using System.Globalization;

namespace airport
{
    class AirportWithArrays
    {
        public enum DepartureStatus
        {
            Check_in = 1,
            Gate_open,
            Departed_at_,
            In_flight,
            Unknown,
            Canceled,
            Delayed
        };
        public enum ArrivalStatus
        {
            Expected_at_ = 1,
            Arrived_at_,
            Unknown,
            Canceled,
            Delayed
        };
        public enum Airport
        {
            ALG,
            AAE,
            BLJ,
            BJA,
            BSK,
            CFK,
            CZL,
            HME,
            GJL,
            ORN,
            QSF
        };
        public enum Airline
        {
            Aegean_Airlines,
            Aer_Lingus,
            Aeroflot,
            Air_China,
            Aeromexico,
            Air_Arabia,
            Air_Astana,
            Air_Austral,
            Air_Baltic,
            Air_Belgium,
            Air_Canada
        };
        public enum Terminal
        {
            A, B, C, D, E, F
        };
        public enum Operations
        {
            Input=1,
            Edit,
            Delete,
            Search,
            Output_of_emergency,
            Exit
        };
        public enum FlightType
        {
            Departure =1,
            Arrival     
        };
        public enum ParametersToEdit
        {
            Date = 1,
            Time,
            Airport,
            Airline,
            Terminal,
            Gate,
            Status
        };
        public enum ParametersToSearchBy
        {
            Flight_Number=1,
            Arrival_time,
            Arrival_Port,
            Departure_Port,
            NearestTimePortFlight
        };

        public enum Emergency
        {
            Fire=1,
            Flood,
            Technical_problems
        };

        static bool IsValidFlightNumber(string input)
        {
            return
            input.Length == 6 &&
            char.IsLetter(input, 0) &&
            char.IsUpper(input, 0) &&
            char.IsLetter(input, 1) &&
            char.IsUpper(input, 1) &&
            char.IsDigit(input, 2) &&
            char.IsDigit(input, 3) &&
            char.IsDigit(input, 4) &&
            char.IsDigit(input, 5);
        }

        static bool IsUnique(string input, string[] source)
        {
            foreach (var item in source)
            {
                if (input.Equals(item))
                {
                    return false;
                }
            }
        return true;
        }


        static void Main(string[] args)
        {
            int MaxDepartureFlightCount = 100;
            int MaxArrivalFlightCount = 100;
            int FilledDepartureFlightCount = 10;
            int FilledArrivalFlightCount = 10;

            const int MaxGateCount = 10;

            //console setup
            Console.SetWindowSize(160, 50);
            Console.Title = "Airport Panel";

            //headlines for timetables
            const int indent = -20;

            string HeadLineArrival =
                $"{"Arrival date",indent}" +
                $"{"Arrival time",indent}" +
                $"{"Flight number",indent}" +
                $"{"Airport",indent}" +
                $"{"Airline",indent}" +
                $"{"Terminal",indent}" +
                $"{"Gate",indent}" +
                $"{"Status",indent}";

            string HeadLineDeparture =
                $"{"Departure date",indent}" +
                $"{"Departure time",indent}" +
                $"{"Flight number",indent}" +
                $"{"Airport",indent}" +
                $"{"Airline",indent}" +
                $"{"Terminal",indent}" +
                $"{"Gate",indent}" +
                $"{"Status",indent}";

            var DepartureDateTimeArray = new DateTime[MaxDepartureFlightCount];
            var DepartureFlightNumberArray = new string[MaxDepartureFlightCount];
            var DepartureAirportArray = new Airport[MaxDepartureFlightCount];
            var DepartureAirlineArray = new Airline[MaxDepartureFlightCount];
            var DepartureTerminalArray = new Terminal[MaxDepartureFlightCount];
            var DepartureGateArray = new string[MaxDepartureFlightCount];
            var DepartureStatusArray = new DepartureStatus[MaxDepartureFlightCount];
            var DepartureStatusTimeArray = new DateTime[MaxDepartureFlightCount];

            var ArrivalDateTimeArray = new DateTime[MaxArrivalFlightCount];
            var ArrivalFlightNumberArray = new string[MaxArrivalFlightCount];
            var ArrivalAirportArray = new Airport[MaxArrivalFlightCount];
            var ArrivalAirlineArray = new Airline[MaxArrivalFlightCount];
            var ArrivalTerminalArray = new Terminal[MaxArrivalFlightCount];
            var ArrivalGateArray = new string[MaxArrivalFlightCount];
            var ArrivalStatusArray = new ArrivalStatus[MaxArrivalFlightCount];
            var ArrivalStatusTimeArray = new DateTime[MaxArrivalFlightCount];

            var Rand = new Random();

            //generating data for departure panel
            for (int DepartureCount = 0; DepartureCount < FilledDepartureFlightCount; DepartureCount++)
            {
                //time and date generation
                //1440 - minutes in day 
                DepartureDateTimeArray[DepartureCount] = DateTime.Now.AddMinutes(Rand.Next(1440));

                //Flight number generation
                //65-90 - Upper Letters
                char FirstLetter = (char)Rand.Next(65, 90);
                char SecondLetter = (char)Rand.Next(65, 90);
                string TwoLetters = FirstLetter.ToString() + SecondLetter.ToString();

                string NumberFlight = Rand.Next(10).ToString() +
                                      Rand.Next(10).ToString() +
                                      Rand.Next(10).ToString() +
                                      Rand.Next(10).ToString();
                
                DepartureFlightNumberArray[DepartureCount] = TwoLetters + NumberFlight;

                //Airport
                DepartureAirportArray[DepartureCount] =
                    (Airport)Rand.Next(Enum.GetValues(typeof(Airport)).Length);
                //Airline
                DepartureAirlineArray[DepartureCount] = 
                    (Airline)Rand.Next(Enum.GetValues(typeof(Airline)).Length);
                //Terminal
                DepartureTerminalArray[DepartureCount] =
                    (Terminal)Rand.Next(Enum.GetValues(typeof(Terminal)).Length);
                //Gate
                DepartureGateArray[DepartureCount] = 
                    DepartureTerminalArray[DepartureCount].ToString() +
                    Rand.Next(1,MaxGateCount).ToString();
                //Status
                DepartureStatusArray[DepartureCount] =
                    (DepartureStatus)Rand.Next(1, Enum.GetNames(typeof(DepartureStatus)).Length+1);
                switch (DepartureStatusArray[DepartureCount])
                {
                    case DepartureStatus.Departed_at_:
                        {
                            DepartureStatusTimeArray[DepartureCount] = DepartureDateTimeArray[DepartureCount].
                                AddMinutes(Rand.Next(10));
                            break;
                        }
                }
            }
            //generating data for arrival panel
            for (int ArrivalCount = 0; ArrivalCount < FilledArrivalFlightCount; ArrivalCount++)
            {
                //time and date generation
                //1440 - minutes in day 
                ArrivalDateTimeArray[ArrivalCount] = DateTime.Now.AddMinutes(Rand.Next(1440));
                //Flight number generation
                //65-90 - Upper Letters
                char FirstLetter = (char)Rand.Next(65, 90);
                char SecondLetter = (char)Rand.Next(65, 90);
                string TwoLetters = FirstLetter.ToString() + SecondLetter.ToString();

                string NumberFlight = Rand.Next(10).ToString() +
                                      Rand.Next(10).ToString() +
                                      Rand.Next(10).ToString() +
                                      Rand.Next(10).ToString();

                ArrivalFlightNumberArray[ArrivalCount] = TwoLetters + NumberFlight;
                //Airport
                ArrivalAirportArray[ArrivalCount] =
                    (Airport)Rand.Next(Enum.GetValues(typeof(Airport)).Length);
                //Airline
                ArrivalAirlineArray[ArrivalCount] =
                    (Airline)Rand.Next(Enum.GetValues(typeof(Airline)).Length);
                //Terminal
                ArrivalTerminalArray[ArrivalCount] = 
                    (Terminal)Rand.Next(Enum.GetValues(typeof(Terminal)).Length);
                //Gate
                ArrivalGateArray[ArrivalCount] =
                    ArrivalTerminalArray[ArrivalCount].ToString() +
                    Rand.Next(1, MaxGateCount).ToString();
                //Status
                ArrivalStatusArray[ArrivalCount] =
                    (ArrivalStatus)Rand.Next(1, Enum.GetNames(typeof(ArrivalStatus)).Length + 1);

                switch (ArrivalStatusArray[ArrivalCount])
                {
                    case ArrivalStatus.Arrived_at_:
                        {
                            //retard or overtake
                            var rand = Rand.Next(2);
                            if(rand==0)
                            {
                                ArrivalStatusTimeArray[ArrivalCount] = ArrivalDateTimeArray[ArrivalCount].
                                    AddMinutes(Rand.Next(10));
                                break;
                            }
                            else
                            {
                                var TimeSpan = new TimeSpan(0, Rand.Next(10), 0);
                                ArrivalStatusTimeArray[ArrivalCount] = ArrivalDateTimeArray[ArrivalCount].
                                    Subtract(TimeSpan);
                                break;
                            }
                        }
                    case ArrivalStatus.Expected_at_:
                        {
                            //retard or overtake
                            var rand = Rand.Next(2);
                            if (rand == 0)
                            {
                                ArrivalStatusTimeArray[ArrivalCount] = ArrivalDateTimeArray[ArrivalCount].
                                    AddMinutes(Rand.Next(10));
                                break;
                            }
                            else
                            {
                                var TimeSpan = new TimeSpan(0, Rand.Next(10), 0);
                                ArrivalStatusTimeArray[ArrivalCount] = ArrivalDateTimeArray[ArrivalCount].
                                    Subtract(TimeSpan);
                                break;
                            }
                        }
                }
            }

            bool IsExit = false;
            do
            {
                //displaying departure panel data
                Console.WriteLine(HeadLineDeparture);
                for (int i = 0; i < FilledDepartureFlightCount; i++)
                {
                    Console.Write(
                        $"{DepartureDateTimeArray[i].ToShortDateString(),indent}" +
                        $"{DepartureDateTimeArray[i].ToShortTimeString(),indent}" +
                        $"{DepartureFlightNumberArray[i],indent}" +
                        $"{DepartureAirportArray[i],indent}" +
                        $"{DepartureAirlineArray[i],indent}" +
                        $"{DepartureTerminalArray[i],indent}" +
                        $"{DepartureGateArray[i],indent}");
                    switch (DepartureStatusArray[i])
                    {
                        case DepartureStatus.Departed_at_:
                            {
                                string result = 
                                    DepartureStatusArray[i] + 
                                    DepartureStatusTimeArray[i].ToShortTimeString();
                                Console.WriteLine($"{result,indent}");
                                break;
                            }
                        default:
                            Console.WriteLine($"{DepartureStatusArray[i],indent}");
                            break;
                    }
                }

                Console.WriteLine("------------------------------------------------");

                //displaying arrival panel data
                Console.WriteLine(HeadLineArrival);
                for (int i = 0; i < FilledArrivalFlightCount; i++)
                {
                    Console.Write(
                        $"{ArrivalDateTimeArray[i].ToShortDateString(),indent}" +
                        $"{ArrivalDateTimeArray[i].ToShortTimeString(),indent}" +
                        $"{ArrivalFlightNumberArray[i],indent}" +
                        $"{ArrivalAirportArray[i],indent}" +
                        $"{ArrivalAirlineArray[i],indent}" +
                        $"{ArrivalTerminalArray[i],indent}" +
                        $"{ArrivalGateArray[i],indent}");
                    switch (ArrivalStatusArray[i])
                    {
                        case ArrivalStatus.Expected_at_:
                            {
                                string result = ArrivalStatusArray[i].ToString() +
                                                ArrivalStatusTimeArray[i].ToShortTimeString();
                                Console.WriteLine($"{result,indent}");
                                break;
                            }
                        case ArrivalStatus.Arrived_at_:
                            {
                                string result = ArrivalStatusArray[i].ToString() + 
                                                ArrivalStatusTimeArray[i].ToShortTimeString();
                                Console.WriteLine($"{result,indent}");
                                break;
                            }
                        default:
                            Console.WriteLine($"{ArrivalStatusArray[i],indent}");
                            break;
                    }
                }

                //Operation input and the validation
                bool IsValidInput = false;
                Operations operation = Operations.Exit;
                do
                {
                    foreach (var EnumItem in Enum.GetValues(typeof(Operations)))
                    {
                        Console.WriteLine($"{(int)EnumItem}) {EnumItem}");
                    }
                    Console.Write("Choose an operation (enter number): ");

                    dynamic InputOperation = Console.ReadLine();
                    if(int.TryParse(InputOperation, out int result))
                    {
                        IsValidInput = true;
                        operation = (Operations)Enum.ToObject(typeof(Operations), result);
                        break;
                    }
                    if(Enum.IsDefined(typeof(Operations), InputOperation))
                    {
                        IsValidInput = true;
                        operation = InputOperation;
                    }
                    else
                    {
                        Console.WriteLine("Wrong input");
                    }
                } while (!IsValidInput);

                switch (operation)
                {
                    case Operations.Input:
                        {
                            bool IsValidInputFlightType = false;
                            FlightType FlightType = FlightType.Departure;
                            do
                            {
                                foreach(var item in Enum.GetValues(typeof(FlightType)))
                                {
                                    Console.WriteLine($"{(int)item}) {item}");
                                }
                                Console.Write("flight type: ");

                            dynamic InputFlightType = Console.ReadLine();

                                if (Enum.IsDefined(typeof(FlightType), InputFlightType))
                                {
                                    IsValidInputFlightType = true;
                                    FlightType = Enum.Parse(typeof(FlightType), InputFlightType);
                                }
                                else
                                {
                                    Console.WriteLine("Wrong input. Try again");
                                }
                            } while (!IsValidInputFlightType);

                            switch (FlightType)
                            {
                                case FlightType.Departure:
                                    {
                                        //Input Date and its parse
                                        bool IsValidInputDate = false;
                                        do
                                        {
                                            Console.Write("Enter flight date and time (11.11.2000 11:11): ");
                                            if (DateTime.TryParseExact(
                                                Console.ReadLine(),
                                                "g",
                                                DateTimeFormatInfo.CurrentInfo,
                                                DateTimeStyles.None,
                                                out DateTime result))
                                            {
                                                IsValidInputDate = true;
                                                DepartureDateTimeArray[FilledDepartureFlightCount] = result;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input");
                                            }
                                        } while (!IsValidInputDate);

                                        //input flight number and its validation
                                        bool IsValidInputFlightNumber = false;
                                        do
                                        {
                                            Console.Write("Enter flight number: ");
                                            var InputFlightNumber = Console.ReadLine();

                                            if (IsValidFlightNumber(InputFlightNumber) &&
                                                IsUnique(InputFlightNumber, DepartureFlightNumberArray))
                                            {
                                                IsValidInputFlightNumber = true;
                                                DepartureFlightNumberArray[FilledDepartureFlightCount] = InputFlightNumber;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong flight number format or it's not unique");
                                            }
                                        } while (!IsValidInputFlightNumber);

                                        //input airport and its validation
                                        bool IsValidInputAirport = false;
                                        do
                                        {
                                            Console.Write("Enter airport: ");
                                            dynamic InputAirport = Console.ReadLine();
                                            if (Enum.IsDefined(typeof(Airport), InputAirport))
                                            {
                                                IsValidInputAirport = true;
                                                DepartureAirportArray[FilledDepartureFlightCount] =
                                                    (Airport)Enum.Parse(typeof(Airport),InputAirport);
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input or there's no such airport");
                                            }
                                        } while (!IsValidInputAirport);

                                        //input airline and its validation
                                        bool IsValidInputAirline = false;
                                        do
                                        {
                                            Console.Write("Enter airline: ");
                                            dynamic InputAirline = Console.ReadLine();
                                            if (Enum.IsDefined(typeof(Airline), InputAirline))
                                            {
                                                IsValidInputAirline = true;
                                                DepartureAirlineArray[FilledDepartureFlightCount] =
                                                    (Airline)Enum.Parse(typeof(Airline),InputAirline);
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input or There's no such airline");
                                            }
                                        } while (!IsValidInputAirline);

                                        //input termial and its validation
                                        bool IsValidInputTerminal = false;
                                        do
                                        {
                                        Console.Write("enter terminal: ");
                                        dynamic InputTerminal = Console.ReadLine();
                                            if (Enum.IsDefined(typeof(Terminal), InputTerminal))
                                            {
                                                IsValidInputTerminal = true;
                                                DepartureTerminalArray[FilledDepartureFlightCount] =
                                                    (Terminal)Enum.Parse(typeof(Terminal),InputTerminal);
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input");
                                            }

                                        } while (!IsValidInputTerminal);

                                        //input gate and its validation
                                        bool IsValidInputGate = false;
                                        do
                                        {
                                            Console.Write("enter gate: ");
                                            var InputGate = Console.ReadLine();

                                            if (char.IsDigit(InputGate, 0))
                                            {
                                                IsValidInputGate = true;
                                                DepartureGateArray[FilledDepartureFlightCount] =
                                                    DepartureTerminalArray[FilledArrivalFlightCount] + InputGate;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input");
                                            }
                                        } while (!IsValidInputGate);

                                        //input status and its validation
                                        bool IsValidInputStatus = false;
                                        do
                                        {
                                            foreach (var EnumItem in Enum.GetValues(typeof(DepartureStatus)))
                                            {
                                                Console.WriteLine($"{(int)EnumItem}) {EnumItem}");
                                            }

                                            Console.Write("Enter flight status: ");

                                            dynamic InputStatus = Console.ReadLine();
                                            if (Enum.IsDefined(typeof(DepartureStatus),InputStatus))
                                            {
                                                InputStatus =
                                                    (DepartureStatus)Enum.Parse(typeof(DepartureStatus),InputStatus);
                                                switch (InputStatus)
                                                {
                                                    case DepartureStatus.Departed_at_:
                                                    {
                                                        bool IsValidInputStatusTime = false;
                                                        do
                                                        {
                                                            Console.Write("Enter time: ");
                                                            if (DateTime.TryParseExact
                                                                (Console.ReadLine(),
                                                                "t",
                                                                DateTimeFormatInfo.CurrentInfo,
                                                                DateTimeStyles.None,
                                                                out DateTime result))
                                                            {
                                                                IsValidInputStatus = true;
                                                                IsValidInputStatusTime = true;
                                                                    DepartureStatusArray[FilledDepartureFlightCount] = InputStatus;
                                                                    DepartureStatusTimeArray[FilledDepartureFlightCount] = result;
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("Wrong input");
                                                            }
                                                        } while (!IsValidInputStatusTime);
                                                        break;
                                                    }
                                                    default:
                                                    {
                                                        IsValidInputStatus = true;
                                                        DepartureStatusArray[FilledArrivalFlightCount] = InputStatus;
                                                        break;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input");
                                            }
                                        } while (!IsValidInputStatus);

                                        FilledDepartureFlightCount++;
                                        Console.Clear();
                                        break;
                                    }
                                case FlightType.Arrival:
                                    {
                                        //Input Date and its parse
                                        bool IsValidInputDate = false;
                                        do
                                        {
                                            Console.Write("Enter flight date and time (11.11.2000 11:11): ");
                                            if (DateTime.TryParseExact(
                                                Console.ReadLine(),
                                                "g",
                                                DateTimeFormatInfo.CurrentInfo,
                                                DateTimeStyles.None,
                                                out DateTime result))
                                            {
                                                IsValidInputDate = true;
                                                ArrivalDateTimeArray[FilledDepartureFlightCount] = result;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input");
                                            }
                                        } while (!IsValidInputDate);

                                        //input flight number and its validation
                                        bool IsValidInputFlightNumber = false;
                                        do
                                        {
                                        Console.Write("Enter flight number: ");
                                        var InputFlightNumber = Console.ReadLine();

                                            if (IsValidFlightNumber(InputFlightNumber) &&
                                                IsUnique(InputFlightNumber,ArrivalFlightNumberArray))
                                            {
                                                IsValidInputFlightNumber = true;
                                                ArrivalFlightNumberArray[FilledArrivalFlightCount] = InputFlightNumber;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong flight number format or it's not unique");
                                            }
                                        } while (!IsValidInputFlightNumber);

                                        //input airport and its validation
                                        bool IsValidInputAirport = false;
                                        do
                                        {
                                        Console.Write("Enter airport: ");
                                        dynamic InputAirport = Console.ReadLine();
                                            if (Enum.IsDefined(typeof(Airport), InputAirport))
                                            {
                                                IsValidInputAirport = true;
                                                ArrivalAirportArray[FilledArrivalFlightCount] =
                                                    (Airport)Enum.Parse(typeof(Airport),InputAirport);
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input, null or empty or there's no such airport");
                                            }
                                        } while (!IsValidInputAirport);

                                        //input airline and its validation
                                        bool IsValidInputAirline = false;
                                        do
                                        {
                                            Console.Write("Enter airline: ");
                                            dynamic InputAirline = Console.ReadLine();
                                            if (Enum.IsDefined(typeof(Airline), InputAirline))
                                            {
                                                IsValidInputAirline = true;
                                                ArrivalAirlineArray[FilledArrivalFlightCount] =
                                                    (Airline)Enum.Parse(typeof(Airline),InputAirline);
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input or There's no such airline");
                                            }
                                        } while (!IsValidInputAirline);

                                        //input termial and its validation
                                        bool IsValidInputTerminal = false;
                                        do
                                        {
                                            Console.Write("enter terminal: ");
                                            dynamic InputTerminal = Console.ReadLine();

                                            if (Enum.IsDefined(typeof(Terminal), InputTerminal))
                                            {
                                                IsValidInputTerminal = true;
                                                DepartureTerminalArray[FilledArrivalFlightCount] =
                                                    (Terminal)Enum.Parse(typeof(Terminal),InputTerminal);
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input");
                                            }

                                        } while (!IsValidInputTerminal);

                                        //input gate and its validation
                                        bool IsValidInputGate = false;
                                        do
                                        {
                                            Console.Write($"enter gate: (number < {MaxGateCount}) ");
                                            string InputGate = Console.ReadLine();

                                            if (char.IsDigit(InputGate, 0)  &&
                                                int.Parse(InputGate) < MaxGateCount)
                                            {
                                                IsValidInputGate = true;
                                                ArrivalGateArray[FilledArrivalFlightCount] =
                                                    ArrivalTerminalArray[FilledArrivalFlightCount] + InputGate;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input");
                                            }
                                        } while (!IsValidInputGate);

                                        //input status and its validation
                                        bool IsValidInputStatus = false;
                                        do
                                        {
                                            foreach (var EnumItem in Enum.GetValues(typeof(ArrivalStatus)))
                                            {
                                                Console.WriteLine($"{(int)EnumItem}) {EnumItem}");
                                            }
                                            Console.Write("Enter flight status: ");
                                            dynamic InputStatus = Console.ReadLine();

                                            if (Enum.IsDefined(typeof(ArrivalStatus), InputStatus))
                                            {
                                                InputStatus = 
                                                    (ArrivalStatus)Enum.Parse(typeof(ArrivalStatus), InputStatus);

                                                switch (InputStatus)
                                                {
                                                    case ArrivalStatus.Arrived_at_:
                                                    {
                                                        bool IsValidInputTime = false;
                                                        do
                                                        {
                                                            Console.Write("Enter time: ");
                                                            if (DateTime.TryParseExact(
                                                                Console.ReadLine(),
                                                                "t",
                                                                CultureInfo.CurrentCulture,
                                                                DateTimeStyles.None,
                                                                out DateTime result))
                                                            {
                                                                IsValidInputStatus = true;
                                                                IsValidInputTime = true;
                                                                ArrivalStatusArray[FilledArrivalFlightCount] = InputStatus;
                                                                ArrivalStatusTimeArray[FilledArrivalFlightCount] = result;
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("Wrong input");
                                                            }
                                                        } while (!IsValidInputTime);
                                                        break;
                                                    }
                                                    case ArrivalStatus.Expected_at_:
                                                    {
                                                        bool IsValidInputTime = false;
                                                        do
                                                        {
                                                            Console.Write("Enter time: ");
                                                            if (DateTime.TryParseExact(
                                                                Console.ReadLine(),
                                                                "t",
                                                                CultureInfo.CurrentCulture,
                                                                DateTimeStyles.None,
                                                                out DateTime result))
                                                            {
                                                                IsValidInputStatus = true;
                                                                IsValidInputTime = true;
                                                                ArrivalStatusArray[FilledArrivalFlightCount] = InputStatus;
                                                                ArrivalStatusTimeArray[FilledArrivalFlightCount] = result;
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("Wrong input");
                                                            }
                                                        } while (!IsValidInputTime);
                                                        break;
                                                    }
                                                    default:
                                                    {
                                                        IsValidInputStatus = true;
                                                        ArrivalStatusArray[FilledArrivalFlightCount] = InputStatus;
                                                        break;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input");
                                            }
                                        } while (!IsValidInputStatus);

                                        FilledArrivalFlightCount++;
                                        Console.Clear();
                                        break;
                                    }
                            }
                    break;
                        }
                    case Operations.Edit:
                        {
                            //input flight number, its validations, search in panels
                            bool IsFound = false;
                            bool IsDepartureFlightNumber = false;
                            int index = 0;
                            do
                            {
                                bool IsValidInputFlightNumber = false;
                                string FlightNumber = string.Empty;
                                do
                                {
                                    Console.Write("Enter flight number: ");
                                    string FlightNumberInput = Console.ReadLine();
                                    if (IsValidFlightNumber(FlightNumberInput))
                                    {
                                        IsValidInputFlightNumber = true;
                                        FlightNumber = FlightNumberInput;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Wrong input");
                                    }
                                } while (!IsValidInputFlightNumber);

                                //search FlightNumber in Departure panel
                                IsDepartureFlightNumber = false;
                                for (int item = 0; item < FilledDepartureFlightCount; item++)
                                {
                                    if (DepartureFlightNumberArray[item].Equals(FlightNumber))
                                    {
                                        IsFound = true;
                                        IsDepartureFlightNumber = true;
                                        index = item;
                                        break;
                                    }
                                }
                                //search FlightNumber in Arrival panel
                                if (!IsFound)
                                {
                                    for (int item = 0; item < FilledArrivalFlightCount; item++)
                                    {
                                        if (ArrivalFlightNumberArray[item].Equals(FlightNumber))
                                        {
                                            IsFound = true;
                                            IsDepartureFlightNumber = false;
                                            index = item;
                                            break;
                                        }
                                    }
                                }
                                if (!IsFound)
                                    Console.WriteLine("The flight number was not found. Try Again");
                            } while (!IsFound);

                            //input parameter to edit, its validation
                            IsValidInput = false;
                            ParametersToEdit EditParameter = ParametersToEdit.Time;
                            do
                            {
                                foreach (var item in Enum.GetValues(typeof(ParametersToEdit)))
                                {
                                    Console.WriteLine($"{(int)item}) {item}");
                                }
                                Console.Write("What parameter do you want to edit? : ");

                                //input Parameter to edit
                                var input = Console.ReadLine();

                                if (Enum.IsDefined(typeof(ParametersToEdit), input))
                                {
                                    IsValidInput = true;
                                    EditParameter =
                                        (ParametersToEdit)Enum.Parse(typeof(ParametersToEdit), input);
                                }
                                else
                                {
                                    Console.WriteLine("Wrong input");
                                    IsValidInput = false;
                                }

                            } while (!IsValidInput);

                            bool IsValidInputParameter;

                            switch (EditParameter)
                            {
                                case ParametersToEdit.Date:
                                    IsValidInputParameter = false;
                                    do
                                    {
                                        Console.Write($"New value for {EditParameter}: ");
                                        string input = Console.ReadLine();
                                        if(DateTime.TryParseExact(
                                            input,
                                            "d",
                                            CultureInfo.CurrentCulture,
                                            DateTimeStyles.None,
                                            out DateTime result))
                                        {
                                            IsValidInputParameter = true;
                                            if (IsDepartureFlightNumber)
                                            {
                                                TimeSpan TimeComponent = DepartureDateTimeArray[index].TimeOfDay;
                                                DepartureDateTimeArray[index] = result;
                                                DepartureDateTimeArray[index] += TimeComponent;
                                            }
                                            else
                                            {
                                                TimeSpan TimeComponent = ArrivalDateTimeArray[index].TimeOfDay;
                                                ArrivalDateTimeArray[index] = result;
                                                ArrivalDateTimeArray[index] += TimeComponent;
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("Wrong input");
                                        }
                                    } while (!IsValidInputParameter);
                                    break;
                                case ParametersToEdit.Time:
                                    IsValidInputParameter = false;
                                    do
                                    {
                                        Console.Write($"New value for {EditParameter}: ");
                                        string input = Console.ReadLine();

                                        if (DateTime.TryParseExact(
                                            input,
                                            "t",
                                            CultureInfo.CurrentCulture,
                                            DateTimeStyles.None,
                                            out DateTime result))
                                        {
                                            IsValidInputParameter = true;
                                            if (IsDepartureFlightNumber)
                                            {
                                                TimeSpan TimeComponent = DepartureDateTimeArray[index].TimeOfDay;
                                                DepartureDateTimeArray[index] = 
                                                    DepartureDateTimeArray[index].Subtract(TimeComponent);
                                                DepartureDateTimeArray[index] = 
                                                    DepartureDateTimeArray[index].Add(result.TimeOfDay);
                                            }
                                            else
                                            {
                                                TimeSpan TimeComponent = ArrivalDateTimeArray[index].TimeOfDay;
                                                ArrivalDateTimeArray[index] =
                                                    ArrivalDateTimeArray[index].Subtract(TimeComponent);
                                                ArrivalDateTimeArray[index] =
                                                    ArrivalDateTimeArray[index].Add(result.TimeOfDay);
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("Wrong input");
                                            IsValidInput = false;
                                        }



                                    } while (!IsValidInputParameter);
                                    break;
                                case ParametersToEdit.Airport:
                                    IsValidInputParameter = false;
                                    do
                                    {
                                        Console.Write($"New value for {EditParameter}: ");
                                        dynamic input = Console.ReadLine();

                                        if (Enum.IsDefined(typeof(Airport), input))
                                        {
                                            IsValidInputParameter = true;
                                            input = (Airport)Enum.Parse(typeof(Airport), input);
                                            if(IsDepartureFlightNumber)
                                            {
                                                DepartureAirportArray[index] = input;
                                            }
                                            else
                                            {
                                                ArrivalAirportArray[index] = input;
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("Wrong input");
                                        }
                                    } while (!IsValidInputParameter);
                                    break;
                                case ParametersToEdit.Airline:
                                    IsValidInputParameter = false;
                                    do
                                    {
                                        Console.Write($"New value for {EditParameter}: ");
                                        dynamic input = Console.ReadLine();
                                        if (Enum.IsDefined(typeof(Airline), input))
                                        {
                                            IsValidInputParameter = true;
                                            input = (Airline)Enum.Parse(typeof(Airline), input);
                                            if (IsDepartureFlightNumber)
                                            {
                                                DepartureAirlineArray[index] = input;
                                            }
                                            else
                                            {
                                                ArrivalAirlineArray[index] = input;
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("Wrong input");
                                        }
                                    } while (!IsValidInputParameter);
                                    break;
                                case ParametersToEdit.Terminal:
                                    IsValidInputParameter = false;
                                    do
                                    {
                                        Console.Write($"New value for {EditParameter}: ");
                                        dynamic input = Console.ReadLine();
                                        if (Enum.IsDefined(typeof(Terminal), input))
                                        {
                                            IsValidInputParameter = true;
                                            input = (Terminal)Enum.Parse(typeof(Terminal), input);
                                            if (IsDepartureFlightNumber)
                                            {
                                                DepartureTerminalArray[index] = input;
                                            }
                                            else
                                            {
                                                ArrivalTerminalArray[index] = input;
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("Wrong input");
                                        }
                                    } while (!IsValidInputParameter);
                                    break;
                                case ParametersToEdit.Gate:
                                    IsValidInputParameter = false;
                                    do
                                    {
                                        Console.Write($"New value for {EditParameter}: ");
                                        dynamic input = Console.ReadLine();
                                        if(int.TryParse(input, out int result))
                                        {
                                            IsValidInputParameter = true;
                                            if (IsDepartureFlightNumber)
                                            {
                                                DepartureGateArray[index] = string.Empty;
                                                DepartureGateArray[index] = 
                                                    DepartureTerminalArray[index].ToString() + result;
                                            }
                                            else
                                            {
                                                ArrivalGateArray[index] = string.Empty;
                                                ArrivalGateArray[index] =
                                                    ArrivalTerminalArray[index].ToString() + result;
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("Wrong input");
                                        }
                                    } while (!IsValidInputParameter);
                                    break;
                                case ParametersToEdit.Status:
                                    IsValidInputParameter = false;
                                    do
                                    {
                                        if (IsDepartureFlightNumber)
                                        {
                                            foreach (var EnumItem in Enum.GetValues(typeof(DepartureStatus)))
                                            {
                                                Console.WriteLine($"{(int)EnumItem}) {EnumItem}");
                                            }
                                            Console.Write($"New value for {EditParameter}: ");
                                            dynamic input = Console.ReadLine();

                                            if (Enum.IsDefined(typeof(DepartureStatus), input))
                                            {
                                                IsValidInputParameter = true;
                                                input = (DepartureStatus)Enum.Parse(typeof(DepartureStatus), input);
                                                switch (input)
                                                {
                                                    case DepartureStatus.Departed_at_:
                                                        bool IsValidInputStatusTime = false;
                                                        do
                                                        {
                                                            Console.Write("Enter time: ");
                                                            if (DateTime.TryParseExact
                                                                (Console.ReadLine(),
                                                                "t",
                                                                DateTimeFormatInfo.CurrentInfo,
                                                                DateTimeStyles.None,
                                                                out DateTime result))
                                                            {
                                                                IsValidInputStatusTime = true;
                                                                    DepartureStatusArray[index] = input;
                                                                    DepartureStatusTimeArray[index] = result;
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("Wrong input");
                                                            }
                                                        } while (!IsValidInputStatusTime);
                                                        break;

                                                    default:
                                                        DepartureStatusArray[index] = input;
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input");
                                            }
                                        }
                                        else
                                        {
                                            foreach (var EnumItem in Enum.GetValues(typeof(ArrivalStatus)))
                                            {
                                                Console.WriteLine($"{(int)EnumItem}) {EnumItem}");
                                            }
                                            Console.Write($"New value for {EditParameter}: ");
                                            dynamic input = Console.ReadLine();

                                            if (Enum.IsDefined(typeof(ArrivalStatus), input))
                                            {
                                                IsValidInputParameter = true;
                                                input = (ArrivalStatus)Enum.Parse(typeof(ArrivalStatus), input);
                                                switch (input)
                                                {
                                                    case ArrivalStatus.Arrived_at_:
                                                        bool IsValidInputStatusTime = false;
                                                        do
                                                        {
                                                            Console.Write("Enter time: ");
                                                            if (DateTime.TryParseExact
                                                                (Console.ReadLine(),
                                                                "t",
                                                                DateTimeFormatInfo.CurrentInfo,
                                                                DateTimeStyles.None,
                                                                out DateTime result))
                                                            {
                                                                IsValidInputStatusTime = true;
                                                                ArrivalStatusArray[index] = input;
                                                                ArrivalStatusTimeArray[index] = result;
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("Wrong input");
                                                            }
                                                        } while (!IsValidInputStatusTime);
                                                        break;
                                                    case ArrivalStatus.Expected_at_:
                                                        IsValidInputStatusTime = false;
                                                        do
                                                        {
                                                            Console.Write("Enter time: ");
                                                            if (DateTime.TryParseExact
                                                                (Console.ReadLine(),
                                                                "t",
                                                                DateTimeFormatInfo.CurrentInfo,
                                                                DateTimeStyles.None,
                                                                out DateTime result))
                                                            {
                                                                IsValidInputStatusTime = true;
                                                                ArrivalStatusArray[index] = input;
                                                                ArrivalStatusTimeArray[index] = result;
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("Wrong input");
                                                            }
                                                        } while (!IsValidInputStatusTime);

                                                        break;
                                                    default:
                                                        ArrivalStatusArray[index] = input;
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input");
                                            }
                                        }

                                    } while (!IsValidInputParameter);
                                    break;
                            }
                            Console.Clear();
                            break;
                        }
                    case Operations.Search:
                        IsValidInput = false;
                        do
                        {
                            foreach(var item in Enum.GetValues(typeof(ParametersToSearchBy)))
                            {
                                Console.WriteLine($"{(int)item}) {item}");
                            }
                            Console.Write("Enter the parameter to search by: ");
                            dynamic input = Console.ReadLine();
                            if (Enum.IsDefined(typeof(ParametersToSearchBy), input))
                            {
                                IsValidInput = true;
                                input = (ParametersToSearchBy)Enum.Parse(typeof(ParametersToSearchBy), input);

                                switch (input)
                                {
                                    case ParametersToSearchBy.Flight_Number:
                                        do
                                        {
                                            Console.Write("Enter flight number: ");
                                            dynamic InputFlightNumber = Console.ReadLine();

                                            if (IsValidFlightNumber(InputFlightNumber))
                                            {
                                                //Search
                                                int index=0;
                                                bool IsFound = false;
                                                bool IsDepartureFlightNumber = false;
                                                //search FlightNumber in Departure panel
                                                for (int item = 0; item < FilledDepartureFlightCount; item++)
                                                {
                                                    if (DepartureFlightNumberArray[item].Equals(InputFlightNumber))
                                                    {
                                                        IsFound = true;
                                                        IsDepartureFlightNumber = true;
                                                        index = item;
                                                        break;
                                                    }
                                                }
                                                //search FlightNumber in Arrival panel
                                                if (!IsFound)
                                                {
                                                    //search in Departure Panel
                                                    for (int item = 0; item < FilledArrivalFlightCount; item++)
                                                    {
                                                        if (ArrivalFlightNumberArray[item].Equals(InputFlightNumber))
                                                        {
                                                            IsFound = true;
                                                            index = item;
                                                            break;
                                                        }
                                                    }
                                                }
                                                //information output about flight
                                                if (IsFound)
                                                {
                                                    if (IsDepartureFlightNumber)
                                                    {
                                                        Console.WriteLine(HeadLineDeparture);
                                                        Console.Write(
                                                        $"{DepartureDateTimeArray[index].ToShortDateString(),indent}" +
                                                        $"{DepartureDateTimeArray[index].ToShortTimeString(),indent}" +
                                                        $"{DepartureFlightNumberArray[index],indent}" +
                                                        $"{DepartureAirportArray[index],indent}" +
                                                        $"{DepartureAirlineArray[index],indent}" +
                                                        $"{DepartureTerminalArray[index],indent}" +
                                                        $"{DepartureGateArray[index],indent}");
                                                        switch (DepartureStatusArray[index])
                                                        {
                                                            case DepartureStatus.Departed_at_:
                                                            {
                                                                string result =
                                                                    DepartureStatusArray[index] +
                                                                    DepartureStatusTimeArray[index].ToShortTimeString();
                                                                Console.WriteLine($"{result,indent}");
                                                                break;
                                                            }
                                                            default:
                                                                Console.WriteLine($"{DepartureStatusArray[index],indent}");
                                                                break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine(HeadLineArrival);
                                                        Console.Write(
                                                        $"{ArrivalDateTimeArray[index].ToShortDateString(),indent}" +
                                                        $"{ArrivalDateTimeArray[index].ToShortTimeString(),indent}" +
                                                        $"{ArrivalFlightNumberArray[index],indent}" +
                                                        $"{ArrivalAirportArray[index],indent}" +
                                                        $"{ArrivalAirlineArray[index],indent}" +
                                                        $"{ArrivalTerminalArray[index],indent}" +
                                                        $"{ArrivalGateArray[index],indent}");
                                                        switch (ArrivalStatusArray[index])
                                                        {
                                                            case ArrivalStatus.Expected_at_:
                                                            {
                                                                string result = ArrivalStatusArray[index].ToString() +
                                                                                ArrivalStatusTimeArray[index].ToShortTimeString();
                                                                Console.WriteLine($"{result,indent}");
                                                                break;
                                                            }
                                                            case ArrivalStatus.Arrived_at_:
                                                            {
                                                                string result = ArrivalStatusArray[index].ToString() +
                                                                                ArrivalStatusTimeArray[index].ToShortTimeString();
                                                                Console.WriteLine($"{result,indent}");
                                                                break;
                                                            }
                                                            default:
                                                                Console.WriteLine($"{ArrivalStatusArray[index],indent}");
                                                                break;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("The flight number was not found. Try Again");
                                                }
                                                Console.WriteLine("Enter any key to return to main place");
                                                Console.ReadKey();
                                            }
                                            else
                                            {
                                                Console.WriteLine("Wrong input");
                                            }
                                        } while (!IsValidInput);
                                        break;
                                    case ParametersToSearchBy.Arrival_Port:
                                        {
                                            do
                                            {
                                                Console.Write("Enter Arrival Port: ");
                                                input = Console.ReadLine();
                                                if (Enum.IsDefined(typeof(Airport),input))
                                                {
                                                    IsValidInput = true;
                                                    input = (Airport)Enum.Parse(typeof(Airport), input);

                                                    bool IsFound = false;
                                                    int Count = 0;
                                                    var indexes = new int[FilledDepartureFlightCount];
                                                    //search
                                                    for(int i = 0; i < FilledDepartureFlightCount; i++)
                                                    {
                                                        if (DepartureAirportArray[i].Equals(input))
                                                        {
                                                            IsFound = true;
                                                            indexes[Count]= i;
                                                            Count++;
                                                        }
                                                    }

                                                    Array.Resize(ref indexes, Count);

                                                    if (IsFound)
                                                    {
                                                        Console.WriteLine(HeadLineDeparture);

                                                        foreach (int i in indexes)
                                                        {
                                                            Console.Write(
                                                                $"{DepartureDateTimeArray[i].ToShortDateString(),indent}" +
                                                                $"{DepartureDateTimeArray[i].ToShortTimeString(),indent}" +
                                                                $"{DepartureFlightNumberArray[i],indent}" +
                                                                $"{DepartureAirportArray[i],indent}" +
                                                                $"{DepartureAirlineArray[i],indent}" +
                                                                $"{DepartureTerminalArray[i],indent}" +
                                                                $"{DepartureGateArray[i],indent}");
                                                            switch (DepartureStatusArray[i])
                                                            {
                                                                case DepartureStatus.Departed_at_:
                                                                    {
                                                                        string result =
                                                                            DepartureStatusArray[i] +
                                                                            DepartureStatusTimeArray[i].ToShortTimeString();
                                                                        Console.WriteLine($"{result,indent}");
                                                                        break;
                                                                    }
                                                                default:
                                                                    Console.WriteLine($"{DepartureStatusArray[i],indent}");
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine($"There's no such races that depart from our airport and arrive in {input}");
                                                    }

                                                    Console.WriteLine("Enter any key to return to main page");
                                                    Console.ReadKey();
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Wrong input");
                                                }
                                            } while (!IsValidInput);

                                            break;
                                        }
                                    case ParametersToSearchBy.Departure_Port:
                                        {
                                            do
                                            {
                                                Console.Write("Enter Departure Port: ");
                                                input = Console.ReadLine();
                                                if (Enum.IsDefined(typeof(Airport),input))
                                                {
                                                    input = (Airport)Enum.Parse(typeof(Airport), input);
                                                    IsValidInput = true;
                                                    bool IsFound = false;
                                                    int Count = 0;
                                                    var indexes = new int[FilledArrivalFlightCount];

                                                    //search
                                                    for(int i = 0; i < FilledArrivalFlightCount;i++)
                                                    {
                                                        if(ArrivalAirportArray[i].Equals(input))
                                                        {
                                                            IsFound = true;
                                                            indexes[Count] = i;
                                                            Count++;
                                                        }
                                                    }
                                                    Array.Resize(ref indexes, Count);
                                                    //info output
                                                    if (IsFound)
                                                    {
                                                        Console.WriteLine(HeadLineArrival);
                                                        foreach(int i in indexes)
                                                        {
                                                            Console.Write(
                                                            $"{ArrivalDateTimeArray[i].ToShortDateString(),indent}" +
                                                            $"{ArrivalDateTimeArray[i].ToShortTimeString(),indent}" +
                                                            $"{ArrivalFlightNumberArray[i],indent}" +
                                                            $"{ArrivalAirportArray[i],indent}" +
                                                            $"{ArrivalAirlineArray[i],indent}" +
                                                            $"{ArrivalTerminalArray[i],indent}" +
                                                            $"{ArrivalGateArray[i],indent}");
                                                            switch (ArrivalStatusArray[i])
                                                            {
                                                                case ArrivalStatus.Expected_at_:
                                                                    {
                                                                        string result = ArrivalStatusArray[i].ToString() +
                                                                                        ArrivalStatusTimeArray[i].ToShortTimeString();
                                                                        Console.WriteLine($"{result,indent}");
                                                                        break;
                                                                    }
                                                                case ArrivalStatus.Arrived_at_:
                                                                    {
                                                                        string result = ArrivalStatusArray[i].ToString() +
                                                                                        ArrivalStatusTimeArray[i].ToShortTimeString();
                                                                        Console.WriteLine($"{result,indent}");
                                                                        break;
                                                                    }
                                                                default:
                                                                    Console.WriteLine($"{ArrivalStatusArray[i],indent}");
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine($"There's no such races that were sended to our airport from {input}");
                                                    }

                                                }
                                                else
                                                {
                                                    Console.WriteLine("Wrong input");
                                                }





                                            } while (!IsValidInput);


                                            break;
                                        }
                                    case ParametersToSearchBy.Arrival_time:
                                        {
                                            do
                                            {
                                                int index=0;
                                                bool IsFound=false;
                                                Console.Write("Enter Arrival time: ");
                                                input = Console.ReadLine();
                                                if (DateTime.TryParseExact(
                                                    input,
                                                    "t",
                                                    CultureInfo.CurrentCulture,
                                                    DateTimeStyles.None,
                                                    out DateTime result))
                                                {
                                                    for(int i = 0; i < ArrivalDateTimeArray.Length; i++)
                                                    {
                                                        if (ArrivalDateTimeArray[i].Hour.Equals(result.Hour) &&
                                                            ArrivalDateTimeArray[i].Minute.Equals(result.Minute))
                                                        {
                                                            index = i;
                                                            IsFound = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Wrong input");
                                                }

                                                if (IsFound)
                                                {
                                                    Console.WriteLine(HeadLineArrival);
                                                    Console.Write(
                                                    $"{ArrivalDateTimeArray[index].ToShortDateString(),indent}" +
                                                    $"{ArrivalDateTimeArray[index].ToShortTimeString(),indent}" +
                                                    $"{ArrivalFlightNumberArray[index],indent}" +
                                                    $"{ArrivalAirportArray[index],indent}" +
                                                    $"{ArrivalAirlineArray[index],indent}" +
                                                    $"{ArrivalTerminalArray[index],indent}" +
                                                    $"{ArrivalGateArray[index],indent}");
                                                    switch (ArrivalStatusArray[index])
                                                    {
                                                        case ArrivalStatus.Expected_at_:
                                                            {
                                                                string Status = ArrivalStatusArray[index].ToString() +
                                                                                ArrivalStatusTimeArray[index].ToShortTimeString();
                                                                Console.WriteLine($"{Status,indent}");
                                                                break;
                                                            }
                                                        case ArrivalStatus.Arrived_at_:
                                                            {
                                                                string Status = ArrivalStatusArray[index].ToString() +
                                                                                ArrivalStatusTimeArray[index].ToShortTimeString();
                                                                Console.WriteLine($"{Status,indent}");
                                                                break;
                                                            }
                                                        default:
                                                            Console.WriteLine($"{ArrivalStatusArray[index],indent}");
                                                            break;
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Flight with such arrival time doesn't exist");
                                                }

                                                Console.WriteLine("Enter any key to return to main page");
                                                Console.ReadKey();

                                            } while (!IsValidInput);

                                            break;
                                        }
                                    case ParametersToSearchBy.NearestTimePortFlight:
                                        {
                                            FlightType TargetFlightType=FlightType.Arrival;
                                            do
                                            {
                                                foreach (var item in Enum.GetValues(typeof(FlightType)))
                                                {
                                                    Console.WriteLine($"{(int)item}) {item}");
                                                }
                                                Console.Write("Enter flight type: ");
                                                input = Console.ReadLine();
                                                if (Enum.TryParse(input, out FlightType result))
                                                {
                                                    IsValidInput = true;
                                                    TargetFlightType = result;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Wrong input");
                                                    IsValidInput = false;
                                                }
                                            } while (!IsValidInput);

                                            DateTime TargetTime = new DateTime();
                                            do
                                            {
                                                Console.Write("Enter start time: ");
                                                input = Console.ReadLine();
                                                if(DateTime.TryParseExact(input,"t",
                                                    CultureInfo.CurrentCulture,DateTimeStyles.None,
                                                    out DateTime result))
                                                {
                                                    IsValidInput = true;
                                                    TargetTime = result;   
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Wrong input");
                                                    IsValidInput = false;
                                                }
                                            } while (!IsValidInput);

                                            TimeSpan TimeRange = new TimeSpan(1, 0, 0);
                                            TimeSpan MaxTime = TargetTime.TimeOfDay + TimeRange;

                                            Airport TargetAirport = Airport.AAE;
                                            do
                                            {
                                                if (TargetFlightType == FlightType.Arrival)
                                                {
                                                    Console.Write("Enter Airport from which flight departed: ");
                                                    input = Console.ReadLine();
                                                    if(Enum.TryParse(input, out Airport result))
                                                    {
                                                        IsValidInput = true;
                                                        TargetAirport = result;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Wrong input");
                                                    }
                                                }
                                                if (TargetFlightType == FlightType.Departure)
                                                {
                                                    Console.Write("Enter airport to which flight is flying: ");
                                                    input = Console.ReadLine();

                                                    if(Enum.TryParse(input, out Airport result))
                                                    {
                                                        IsValidInput = true;
                                                        TargetAirport = result;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Wrong input");
                                                        IsValidInput = false;
                                                    }
                                                }
                                            } while (!IsValidInput);

                                            //search
                                            //two arrays (key,value) for departure
                                            var DepartureIndexes = new int[FilledDepartureFlightCount];
                                            var DepartureDateTime = new DateTime[FilledDepartureFlightCount];
                                            //two arrays (key,value) for arrival
                                            var ArrivalIndexes = new int[FilledArrivalFlightCount];
                                            var ArrivalDateTime = new DateTime[FilledArrivalFlightCount];
                                            int Count = 0;
                                            
                                            if (TargetFlightType == FlightType.Arrival)
                                            {
                                                for (int i = 0; i < FilledArrivalFlightCount; i++)
                                                {
                                                    if(ArrivalStatusArray[i] == ArrivalStatus.Expected_at_ &&
                                                       ArrivalAirportArray[i].Equals(TargetAirport) &&
                                                       ArrivalDateTimeArray[i].TimeOfDay < MaxTime  &&
                                                       ArrivalDateTimeArray[i].TimeOfDay > TargetTime.TimeOfDay)
                                                    {
                                                        ArrivalIndexes[Count] = i;
                                                        ArrivalDateTime[Count] = ArrivalDateTimeArray[i];
                                                        Count++;
                                                    }
                                                }
                                                Array.Resize(ref ArrivalIndexes, Count);
                                                Array.Resize(ref ArrivalDateTime, Count);
                                            }
                                            if (TargetFlightType == FlightType.Departure)
                                            {
                                                for(int i = 0; i < FilledDepartureFlightCount; i++)
                                                {
                                                    if (DepartureStatusArray[i] != DepartureStatus.Canceled &&
                                                        DepartureStatusArray[i] != DepartureStatus.Unknown &&
                                                        DepartureAirportArray[i].Equals(TargetAirport) &&
                                                        DepartureDateTimeArray[i].TimeOfDay < MaxTime &&
                                                        DepartureDateTimeArray[i].TimeOfDay > TargetTime.TimeOfDay)
                                                    {
                                                        DepartureIndexes[Count] = i;
                                                        DepartureDateTime[Count] = DepartureDateTimeArray[i];
                                                        Count++;
                                                    }
                                                }
                                                Array.Resize(ref DepartureIndexes, Count);
                                                Array.Resize(ref DepartureDateTime, Count);
                                            }

                                            //sorting
                                            if (TargetFlightType == FlightType.Arrival)
                                            {
                                                //sorting by value (time)
                                                Array.Sort(ArrivalDateTime, ArrivalIndexes);
                                            }
                                            if (TargetFlightType== FlightType.Departure)
                                            {
                                                //sorting by value (time)
                                                Array.Sort(DepartureDateTime, DepartureIndexes);
                                            }

                                            //output
                                            if (TargetFlightType == FlightType.Departure)
                                            {
                                                if(DepartureIndexes.Length==0)
                                                {
                                                    Console.WriteLine($"There's no such races that will be departed in range ({TargetTime.ToShortTimeString()} - {MaxTime}) to {TargetAirport}");
                                                }
                                                else
                                                {
                                                    Console.WriteLine(HeadLineDeparture);
                                                    foreach (var i in DepartureIndexes)
                                                    {
                                                        Console.Write(
                                                        $"{DepartureDateTimeArray[i].ToShortDateString(),indent}" +
                                                        $"{DepartureDateTimeArray[i].ToShortTimeString(),indent}" +
                                                        $"{DepartureFlightNumberArray[i],indent}" +
                                                        $"{DepartureAirportArray[i],indent}" +
                                                        $"{DepartureAirlineArray[i],indent}" +
                                                        $"{DepartureTerminalArray[i],indent}" +
                                                        $"{DepartureGateArray[i],indent}");
                                                        switch (DepartureStatusArray[i])
                                                        {
                                                            case DepartureStatus.Departed_at_:
                                                                {
                                                                    string result =
                                                                        DepartureStatusArray[i] +
                                                                        DepartureStatusTimeArray[i].ToShortTimeString();
                                                                    Console.WriteLine($"{result,indent}");
                                                                    break;
                                                                }
                                                            default:
                                                                Console.WriteLine($"{DepartureStatusArray[i],indent}");
                                                                break;
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            if (TargetFlightType == FlightType.Arrival)
                                            {
                                                if (ArrivalIndexes.Length == 0)
                                                {
                                                    Console.WriteLine($"There's no such races that will be arrived in ({TargetTime.ToShortTimeString()}-{MaxTime}) from {TargetAirport}");
                                                }
                                                else
                                                {
                                                    Console.WriteLine(HeadLineArrival);
                                                    foreach (var i in ArrivalIndexes)
                                                    {
                                                        Console.Write(
                                                        $"{ArrivalDateTimeArray[i].ToShortDateString(),indent}" +
                                                        $"{ArrivalDateTimeArray[i].ToShortTimeString(),indent}" +
                                                        $"{ArrivalFlightNumberArray[i],indent}" +
                                                        $"{ArrivalAirportArray[i],indent}" +
                                                        $"{ArrivalAirlineArray[i],indent}" +
                                                        $"{ArrivalTerminalArray[i],indent}" +
                                                        $"{ArrivalGateArray[i],indent}");
                                                        switch (ArrivalStatusArray[i])
                                                        {
                                                            case ArrivalStatus.Expected_at_:
                                                                {
                                                                    string result = ArrivalStatusArray[i].ToString() +
                                                                                    ArrivalStatusTimeArray[i].ToShortTimeString();
                                                                    Console.WriteLine($"{result,indent}");
                                                                    break;
                                                                }
                                                            case ArrivalStatus.Arrived_at_:
                                                                {
                                                                    string result = ArrivalStatusArray[i].ToString() +
                                                                                    ArrivalStatusTimeArray[i].ToShortTimeString();
                                                                    Console.WriteLine($"{result,indent}");
                                                                    break;
                                                                }
                                                            default:
                                                                Console.WriteLine($"{ArrivalStatusArray[i],indent}");
                                                                break;
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            Console.WriteLine("Enter any key to return to main page");
                                            Console.ReadKey();
                                            Console.Clear();
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                Console.WriteLine("Wrong input");
                            }
                        }
                        while (!IsValidInput);
                        break;
                    case Operations.Delete:
                        {
                            string FlightNumberDelete = string.Empty;
                            IsValidInput = false;
                            do
                            {
                                Console.Write("Enter flight number:");
                                string FlightNumber = Console.ReadLine();

                                if (IsValidFlightNumber(FlightNumber))
                                {
                                    IsValidInput = true;
                                    FlightNumberDelete = FlightNumber;
                                }
                                else
                                {
                                    Console.WriteLine("Wrong flight number format");
                                }
                            } while (!IsValidInput);

                            int index = 0;
                            bool IsFound = false;
                            bool IsDepartureFlight = false;

                            //finding match
                            for (int i = 0; i < DepartureFlightNumberArray.Length; i++)
                            {
                                if (DepartureFlightNumberArray[i] == FlightNumberDelete)
                                {
                                    IsFound = true;
                                    IsDepartureFlight = true;
                                    index = i;
                                    break;
                                }
                            }
                            if (IsFound)
                            {
                                //uplift beneath rows on one point
                                while (DepartureFlightNumberArray[index] != null)
                                {
                                    DepartureDateTimeArray[index] = DepartureDateTimeArray[index + 1];
                                    DepartureFlightNumberArray[index] = DepartureFlightNumberArray[index + 1];
                                    DepartureAirportArray[index] = DepartureAirportArray[index + 1];
                                    DepartureAirlineArray[index] = DepartureAirlineArray[index + 1];
                                    DepartureTerminalArray[index] = DepartureTerminalArray[index + 1];
                                    DepartureGateArray[index] = DepartureGateArray[index + 1];
                                    DepartureStatusArray[index] = DepartureStatusArray[index + 1];
                                    index++;
                                }
                            }
                            //is not found in departure panel, find in arrival panel
                            else
                            {
                                for (int i = 0; i < ArrivalFlightNumberArray.Length; i++)
                                {
                                    if (ArrivalFlightNumberArray[i] == FlightNumberDelete)
                                    {
                                        IsFound = true;
                                        index = i;
                                        break;
                                    }
                                }

                                if (IsFound)
                                {
                                    //uplift beneath rows on one point
                                    while (ArrivalFlightNumberArray[index] != null)
                                    {
                                        ArrivalDateTimeArray[index] = ArrivalDateTimeArray[index + 1];
                                        ArrivalFlightNumberArray[index] = ArrivalFlightNumberArray[index + 1];
                                        ArrivalAirportArray[index] = ArrivalAirportArray[index + 1];
                                        ArrivalAirlineArray[index] = ArrivalAirlineArray[index + 1];
                                        ArrivalTerminalArray[index] = ArrivalTerminalArray[index + 1];
                                        ArrivalGateArray[index] = ArrivalGateArray[index + 1];
                                        ArrivalStatusArray[index] = ArrivalStatusArray[index + 1];
                                        index++;
                                    }
                                }
                            }

                            if (IsFound)
                            {
                                Console.WriteLine($"Flight {FlightNumberDelete} was deleted.");
                                int delay = 5000;
                                Console.WriteLine($"Panel will be refreshed over {delay/1000} seconds.");
                                System.Threading.Thread.Sleep(delay);

                                //don't displaying empty row
                                if (IsDepartureFlight)
                                    FilledDepartureFlightCount--;
                                else
                                    FilledArrivalFlightCount--;

                                Console.Clear();
                            }
                            else
                            {
                                Console.WriteLine("The flight number is not found");
                            }
                            break;
                        }
                    case Operations.Output_of_emergency:
                        {
                            do
                            {
                                foreach (var item in Enum.GetValues(typeof(Emergency)))
                                {
                                    Console.WriteLine($"{(int)item}) {item}");
                                }
                                Console.Write("Enter cause of evacution: ");

                                dynamic input = Console.ReadLine();
                                if (Enum.IsDefined(typeof(Emergency), input))
                                {
                                    IsValidInput = true;
                                    Emergency emergency = Enum.Parse(typeof(Emergency),input);

                                    switch (emergency)
                                    {
                                        case Emergency.Fire:
                                            Console.WriteLine($"Evacution due to {emergency}");
                                            Console.WriteLine("Enter any key to return to main page");
                                            Console.ReadKey();
                                            Console.Clear();
                                            break;
                                        case Emergency.Flood:
                                            Console.WriteLine($"Evacution due to {emergency}");
                                            Console.WriteLine("Enter any key to return to main page");
                                            Console.ReadKey();
                                            Console.Clear();
                                            break;
                                        case Emergency.Technical_problems:
                                            Console.WriteLine($"Evacution due to {emergency}");
                                            Console.WriteLine("Enter any key to return to main page");
                                            Console.ReadKey();
                                            Console.Clear();
                                            break;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Wrong input");
                                    IsValidInput = false;
                                }

                            } while (!IsValidInput);

                            break;
                        }
                    case Operations.Exit:
                        {
                            IsExit = true;
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Wrong input, make another try");
                            break;
                        }
                }
            } while (!IsExit);
        }
    }
}
